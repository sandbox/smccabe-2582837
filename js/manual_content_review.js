(function($) {
    $(document).ready(function () {
        var active = Drupal.settings.manual_content_review.active;

        if (active) {
            $('body').append('<div id="manual-content-review-overlay"><p>Approve?</p><p><span>Yes</span> / <span>No</span></p></div>');
        }

        //Code to add prerender for next page
        $('head').append('<link rel="prerender" href="http://apple.com/ipad">');
    });
})(jQuery);